import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokemonAPIService {

  constructor(private http:HttpClient){}

  getAllPokemons() : Promise<any>{
    return this.http.get(`https://pokeapi.co/api/v2/pokemon?limit=1050&offset=0`).toPromise();
  }
  
  getPokemon(url) : Promise<any>{
    return this.http.get(url).toPromise();
  }

  getPokemonByID(id) : Promise<any>{
    return this.http.get(`https://pokeapi.co/api/v2/pokemon/${id}`).toPromise();
  }

  getPokemons(indx) : Promise<any>{
    return this.http.get(`https://pokeapi.co/api/v2/pokemon?limit=40&offset=${indx}`).toPromise();
  }
  
}
