import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PokemonAPIService } from 'src/app/services/pokemon-api.service';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.css']
})
export class DetailPage implements OnInit {
  id : number = 0; 
  pokemon : any; 

  constructor(private route: ActivatedRoute, private pokeApi: PokemonAPIService, private router : Router) { 
    this.id = Number(this.route.snapshot.paramMap.get('id'));
    this.displayPokemon(this.id);
  }

  async displayPokemon(id){
    await this.pokeApi.getPokemonByID(id).then(pmon => 
        this.pokemon = {
          name: pmon.name,
          height: pmon.height,
          weight: pmon.weight,
          image: pmon.sprites.front_default,
          types: pmon.types,
          stats: pmon.stats,
          moves: pmon.moves,
          abilities: pmon.abilities,
          id: pmon.id,
          basexp: pmon.base_experience,
        }
    );
    console.log(this.pokemon);
  }
  ngOnInit(): void {
  }
  
  catchPokemon(id): void {
    if(localStorage.getItem(String(id))) alert("This pokemon is already in your profile!");
    else{
      localStorage.setItem(String(id), String(id));
      alert('This pokemon is caught and stored in your profile!');
      this.router.navigateByUrl('/catalogue');
    }
  }
}

