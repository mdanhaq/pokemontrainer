import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonAPIService } from '../../../services/pokemon-api.service';

@Component({
  selector: 'app-catalouge',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css']
})
 
export class CataloguePage implements OnInit {
  pokemons : any = [];
  indx : number = 0;
  showLoadingClass : boolean = false;

  constructor(private pokeApi: PokemonAPIService, private router: Router) {}

  async ngOnInit() {
    if(!localStorage.getItem('trainerName')) this.router.navigateByUrl('/');
    this.fetchPokemons(this.indx);
  }
 
  async fetchPokemons(indx) {
    await this.pokeApi.getPokemons(indx).then(res =>
      res.results.forEach(async pmon => {
        await this.pokeApi.getPokemon(pmon.url).then( pinfo =>
          this.insertSorted(this.pokemons,{
            id: pinfo.id,
            name: pinfo.name,
            imgsrc: pinfo.sprites.front_default,
            types: pinfo.types
          }, this.compareById)
        )
      })
    );
  }

  getDetails(id){
    this.router.navigateByUrl(`/details/${id}`);
  }

  insertSorted (array, element, comparator) {
    for (var i = 0; i < array.length && comparator(array[i], element) < 0; i++) {}
    array.splice(i, 0, element)
  }
  
  compareById (a, b) { 
    return a.id - b.id 
  }

  navigateNext(){
    this.indx += 40;
    this.pokemons = [];
    this.fetchPokemons(this.indx);
  }
  navigatePrev(){
    this.indx -= 40;
    this.pokemons = [];
    this.fetchPokemons(this.indx);  
  }
}
