import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonAPIService } from 'src/app/services/pokemon-api.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {

  pokemons : any = [];
  
  constructor(private pokeApi: PokemonAPIService, private router: Router) { }

  ngOnInit(): void {
    this.findAllPokemonIds();    
  }

  findAllPokemonIds() {
    let ids : any = [];
    let keys = Object.keys(localStorage);
    let i = keys.length;

    while ( i-- ) {
      if (keys[i] != 'trainerName') ids.push(Number(localStorage.getItem(keys[i])));
    }
    this.fetchPokemons(ids);
  }

  getDetails(id){
    this.router.navigateByUrl(`/details/${id}`);
  }

  fetchPokemons(ids){
    ids.forEach(id => {
      this.pokeApi.getPokemonByID(id).then(pmon => 
        this.pokemons.push({
          name: pmon.name,
          image: pmon.sprites.front_default,
          id: pmon.id,
          types: pmon.types
        })
      )
    });
  }

  releasePokemon(name, id) {
    localStorage.removeItem(String(id));
    alert(`${name} was released...`);
  }
}
