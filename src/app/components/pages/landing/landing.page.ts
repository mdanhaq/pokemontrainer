import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.css']
})
export class LandingPage implements OnInit {
 
  constructor(private router: Router) { }
  
  public onSubmit(createForm: NgForm): void{
    console.log(createForm.valid);
  }

  getName(name){
    console.log(name);
    localStorage.setItem("trainerName", name);
    this.router.navigateByUrl("/catalogue");
  }

  ngOnInit(): void {
    if(localStorage.getItem("trainerName")){
      this.router.navigateByUrl("/catalogue");
    }
  }
}