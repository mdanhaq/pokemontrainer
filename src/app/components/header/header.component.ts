import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    if (localStorage.getItem("trainerName")) {
      let tname = document.getElementById('trainer-name');
      tname.innerText = localStorage.getItem('trainerName');  
    }
  }

}
