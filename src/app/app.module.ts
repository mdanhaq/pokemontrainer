import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from  '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CataloguePage } from './components/pages/catalogue/catalogue.page';
import { LandingPage } from './components/pages/landing/landing.page';
import { HeaderComponent} from './components/header/header.component';
import { DetailPage } from './components/pages/detail/detail.page';
import { TrainerPage } from './components/pages/trainer/trainer.page';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    CataloguePage,
    LandingPage,
    HeaderComponent,
    DetailPage,
    TrainerPage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
