import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CataloguePage } from './components/pages/catalogue/catalogue.page';
import { DetailPage } from './components/pages/detail/detail.page';
import { LandingPage } from './components/pages/landing/landing.page';
import { TrainerPage } from './components/pages/trainer/trainer.page';

const routes: Routes = [
  {
    path:'', 
    component: LandingPage
  },
  {
    path:'catalogue', 
    component: CataloguePage
  },
  {
    path:'details/:id',
    component: DetailPage
  },
  {
    path:'trainer',
    component: TrainerPage
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
